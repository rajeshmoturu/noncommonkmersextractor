# NoncommonKmersExtractor

Extract all the non-common k-mers, by brute-force method, of a given length among sequences of a given fasta file.

## Usage
From within the repository, 
```
python main.py -s <path-to-input-fasta-file> -o <path-to-output-text-file> [-k <k-mer-size>]
```
* `-s` : Path to the input fasta file.
Input fasta file contains several sequences along with the genome.
* `-o`: Path to the output text file.
Output text file is list of all non-common k-mers between all the pairs of sequences in the input fasta file.
File format:
  ```
  ### for <seq1>, <seq2>:
  List of k-mers from seq1 that are not present in seq2 in input fasta file.
  \n
  ### for <seq1>, <seq3>:
  List of k-mers from seq1 that are not present in seq3 in input fasta file.
  \n
  ...
  ```
* `-k`: Size of the k-mers to be considered (optional parameter).
Default k-mer size is considered to be 25 if none provided exlusively.
