import argparse


def make_kmers(key_value, sequence, kmers_dict, k_mer_len):
    """ build a dictionary with seq as keys and k-mers and as values. """

    k_mers = []
    for i in range(0, len(sequence) - k_mer_len + 1):
        k_mers.append(sequence[i:i + k_mer_len])
    kmers_dict[key_value] = k_mers

    return kmers_dict


def compare_kmers(kmers_dict, output_file):
    """ compare the k-mers of different sequences to output the k-mers that are not common. """

    # iterate over kmers_dict to compare kmers of two given sequences
    non_common_kmers = ""
    for key1 in kmers_dict:
        for key2 in kmers_dict:
            # if they are same sequences, just continue to next iteration
            if key1 is key2:
                continue
            non_common_kmers = non_common_kmers + "\n### for " + key1 + ", " + key2 + ":\n"
            # else, compare the k-mers of the both the sequences to output non-common k-mers
            seqs1, seqs2 = kmers_dict[key1], kmers_dict[key2]
            for seq_ in seqs1:
                if seq_ not in seqs2:
                    non_common_kmers = non_common_kmers + seq_ + "\n"

    # once the string of non-common kmers is built, write it to output file
    with open(output_file, 'a') as of:
        of.write(non_common_kmers)


def arg_parser():
    parser = argparse.ArgumentParser(description="Get non common kmers for a given fasta file.")
    parser.add_argument('-s', '--source-path', help="Provide input fasta file path.", required=True)
    parser.add_argument('-o', '--output-path', help="Provide output text file path.", required=True)
    parser.add_argument('-k', '--kmer-size', help="Length of the k-mers to be considered.", required=False, default=25)
    args = vars(parser.parse_args())

    kmer_size = int(args['kmer_size'])
    input_file = args['source_path']
    output_file = args['output_path']

    return kmer_size, input_file, output_file


if __name__ == '__main__':
    # parse arguments
    k_mer_len, fasta_file_path, output_file_path = arg_parser()

    # read fasta file and extract k-mers
    k_mers_dict = {}
    with open(fasta_file_path, 'r') as f:
        seq = ""
        key = ""

        for line in f.readlines():
            # loop over lines in the fasta file
            if line.startswith(">"):
                # We hit a new sequence
                if key and seq:
                    make_kmers(key, seq, k_mers_dict, k_mer_len)
                key = line[1:].strip()
                seq = ""
            else:
                # Still continuing in the same sequence
                seq += line.strip()

        # Make sure we write the last sequence to the file.
        make_kmers(key, seq, k_mers_dict, k_mer_len)

    # compare k-mers to find out k-mers that can be used to ID a sequence
    compare_kmers(k_mers_dict, output_file_path)
